import React,{Component} from 'react';
import './App.css';
// import Data from './sampleData'
import Board from './components/Board'
import Home from'./components/pages/Home'
import {BrowserRouter, Route,Switch} from 'react-router-dom'
// import PageNotFound from './components/pages/PageNotFound'
import {boardsRef, listsRef,cardsref} from './Firebase'
import AuthProvider from './components/AuthContext'
import UserForm from  './components/UserForm'
import Header from './components/Header'

class  App extends Component {
  state={
    boards:[]
  }
  getBoards=async userId=>{
    try{
this.setState({boards:[]})
const boards=await boardsRef
.where('board.user','==',userId)
.orderBy('board.createdAt')
.get()
boards.forEach(board=>{
  const data=board.data().board
  const boardObj={
    id:board.id,
    ...data
  }
  this.setState({boards:[...this.state.boards,boardObj]})
})
    }catch(error){
      console.log('Error getting boards',error)
    }
  }
  createNewBoard=async board=>{
    try{  
    const newBoard=await boardsRef.add({board})
    const boardObj={
      id:newBoard.id,
      ...board
    }
    this.setState({boards:[...this.state.boards,boardObj]})
  } catch(error){
    console.error('Error Created New Board:', error)
  }
  }
  deleteList=async (listId)=>{
    try{
    const cards=await cardsref
    .where('card.listId', '==',listId)
    .get()
    if(cards.docs.length!==0)
    {
    cards.forEach(card=>{
        card.ref.delete()
    })
    }
    const list=await listsRef.doc(listId)
        list.delete()
    }
    catch(error){
        console.error('Error Deleting List',error)
    }
        }
  deleteBoards=async(boardId)=>{
try{
  const list=await listsRef
  .where('list.board','==',boardId)
  .get()
  if(list.docs.length!==0){
    list.forEach(list=>{
this.deleteList(list.ref.id)
    })
  }
const board=await boardsRef.doc(boardId)
this.setState({
  boards:[...this.state.boards.filter(board=>{
    return board.id!==boardId
  })]
})
board.delete()
}catch(error){
  console.log('deleleted Board Error',error)
}
  }
  updateBoard=async(boardId,newTitle)=>
  {
try{
const board=await boardsRef.doc(boardId)
board.update({'board.title':newTitle})
}catch(error){
  console.error('Error Updating Board:',error)
}
  }
  render(){
  return (
    <div>
      <BrowserRouter>
      <AuthProvider>
        <Header/>
        <Switch>
          <Route exact path="/" component={UserForm}/>
      <Route exact path="/:userID/boards" render={(props)=>(<Home  {...props} getBoards={this.getBoards} boards={this.state.boards} createnewBoard={this.createNewBoard}/>)}/>
      <Route path="/board/:ID" render={(props)=>(<Board {...props}
      deleteBoards={this.deleteBoards}
      updateBoard={this.updateBoard}
      deleteList={this.deleteList}/>)}/>
      {/* <Route Component={PageNotFound}/> */}
  {/* <Home boards={this.state.boards} createnewBoard={this.createNewBoard}/>
  <Board/> */}
  </Switch>
  </AuthProvider>
  </BrowserRouter>
    </div>
  );
}
}

export default App;
