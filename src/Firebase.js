import firebase from 'firebase/app'
import 'firebase/firestore'
import 'firebase/auth'
const config={

    apiKey: "AIzaSyBmWIHJ-wGER_pDG1lToS-KmwUBidHbzgY",
    authDomain: "react-trello-2.firebaseapp.com",
    databaseURL: "https://react-trello-2.firebaseio.com",
    projectId: "react-trello-2",
    storageBucket: "react-trello-2.appspot.com",
    messagingSenderId: "38746621722",
    appId: "1:38746621722:web:dc32a6db1483e467098ea3"
}
firebase.initializeApp(config)
const db=firebase.firestore()
const firebaseAuth=firebase.auth()
const boardsRef=db.collection('boards')
const listsRef=db.collection('lists')
const cardsref=db.collection('cards')
export {boardsRef,listsRef,cardsref,firebaseAuth}
