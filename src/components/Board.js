import React,{Component} from 'react'
import List from './List'
// import Data from '../sampleData'
import {boardsRef,listsRef} from '../Firebase'
import propsTypes from 'prop-types'
import {AuthConsumer} from './AuthContext'
// import { lstat } from 'fs'

class Board extends Component
{
    state={
        currentBoard:{},
        currentList:[],
        message:''
    }

    componentDidMount()
    {
        this.getBoards(this.props.match.params.ID)
        // this.setState({
        //     currentList:Data.lists
        // })
        this.getLists(this.props.match.params.ID)
    }
    getLists=async boardId=>{
        try{
// const lists=
await listsRef
.where('list.board','==',boardId)
.orderBy('list.createdAt')
.onSnapshot(snapshot=>{
    snapshot.docChanges()
    .forEach(change=>{
        if(change.type==='added'){
            const doc=change.doc 
            const list={
                id:doc.id,
                title:doc.data().list.title
            }
            this.setState({
                currentList:[...this.state.currentList,list]
            })
        }
        if(change.type==='removed')
        {
            this.setState({
              currentList: [...this.state.currentList.filter(list=>{
                  return list.id!==change.doc.id
              })]
            })
        }
      
    })
})
// .get()
// lists.forEach(list=>{
//     const data= list.data().list
//     const listObj={
//         id:list.id,
//         ...data
//     }
//     this.setState({
//         currentList:[...this.state.currentList,listObj]
//     })
// })
        }catch(error){console.log('List Created Error',error)}
    }
    getBoards=async boardId=>{
        try{
const board=await boardsRef.doc(boardId).get()
this.setState({currentBoard:board.data().board})
        }catch(error){
            this.setState({
                message:'Board not found...'
            })
        }
    }
    createNewList=async(e,userId)=>{
        try{
e.preventDefault()
        const list={
            title:this.addBoardInput.current.value,
            board:this.props.match.params.ID,
            createdAt:new Date(),
            user:userId
        }
        if(list.title && list.board){
            await listsRef.add({list})
    }
    this.addBoardInput.current.value=''
} catch(error){console.error('Error Created New List',error)}
    }
    addBoardInput=React.createRef()
    deleteBoard=async()=>{
const boardId=this.props.match.params.ID
this.props.deleteBoards(boardId)
this.setState({
    message:'Board not found...'
})
    }
    updateBoard= e =>{
        const boardId=this.props.match.params.ID
        const newTitle=e.currentTarget.value
        if(boardId && newTitle)
        {
this.props.updateBoard(boardId,newTitle)
        }
    }
render()
{
return(
    <AuthConsumer>
    {({user})=>(
        <React.Fragment>
        {user.id===this.state.currentBoard.user?(
        <div className="board-wrapper" style={{backgroundColor:this.state.currentBoard.background}}>
        {/* <spna>{this.props.match.params.ID}</spna> */}
        {/* {user.name} */}
        {this.state.message===''?(
        <div className="board-header">
{/* <h3>{this.state.currentBoard.title}</h3> */} 
<input type="text"name="boardtitle" onChange={this.updateBoard} defaultValue={this.state.currentBoard.title} ></input>
<button onClick={this.deleteBoard}>Delete Board</button>
        </div>
        ):(
            <h2>{this.state.message}</h2>
        )}
    <div className="lists-wrapper">
        {Object.keys(this.state.currentList).map((key)=>(
            <List key={this.state.currentList[key].id}
            deleteList={this.props.deleteList}
            list={this.state.currentList[key]}/>

        ))}
        </div>
        <form onSubmit={(e)=>this.createNewList(e,user.id)} className="new-list-wrapper">
            <input 
            type={this.state.message===''?'text':'hidden'}
            name="name"
            placeholder="+New List"
             ref={this.addBoardInput}></input>
        </form>
        </div>
        ):(<span></span>)}
        </React.Fragment>
    )}
    
        </AuthConsumer>
)

}

}
Board.propsTypes={
    deleteBoards:propsTypes.func.isRequired,
    deleteList:propsTypes.func.isRequired,
    updateBoard:propsTypes.func.isRequired
}
export default Board