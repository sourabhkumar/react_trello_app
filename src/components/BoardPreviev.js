import React,{Component} from 'react'
import propType from'prop-types'
import {withRouter} from 'react-router-dom'
class BoardPreview extends Component
{
    goToBoard=()=>{
const boardId=this.props.board.id
this.props.history.push({
    pathname:`/board/${boardId}`,
    state: {
        title:this.props.board.title,
        background: this.props.board.background
    }
    
})

    }
render()
{
return(
<div>
    <ul className="board-prexiew-item" onClick={this.goToBoard}
    style={{backgroundColor:this.props.board.background}}>
<li>{this.props.board.title}</li>
    </ul>
</div>
)

}

}
BoardPreview.propType={
    board:propType.object.isRequired
}
export default withRouter(BoardPreview)