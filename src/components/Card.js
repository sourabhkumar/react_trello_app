import React,{Component} from 'react'
import propType from'prop-types'
import {cardsref} from '../Firebase'
import EditCardModel from './EditCardModel'
import TextareaAutosize from 'react-autosize-textarea'
class Card extends Component
{
    state={
        modelOpen:false
    }
    toggleModel=()=>{
        this.setState({
            modelOpen:!this.state.modelOpen
        })
    }
    deletecard= async e =>{
        try{
            e.preventDefault()
            const cardId=this.props.data.id
            const card= await cardsref.doc(cardId)
            card.delete()
        }catch(error){
            console.error('Error Deleting Card:',error)
        }
    }
render()
{
return(
<React.Fragment>
<div className="card">
    <div className="card-labels">
        {this.props.data.labels.map(label=>{
            return<span className="label" key={label} style={{backgroundColor:label}}></span>
        })}
    </div>
    <div className="card-body">
    {/* <p onClick={this.toggleModel}>{this.props.data.text}</p> */}
    <TextareaAutosize 
    readOnly
    value={this.props.data.text}
    onClick={this.toggleModel}></TextareaAutosize>
    <span onClick={this.deletecard}>&times;</span>
    </div>
    </div>
    <EditCardModel modelOpen={this.state.modelOpen}
     toggleModel={this.toggleModel}
     cardData={this.props.data}/>
    </React.Fragment>
)

}

}
Card.propType={
    data:propType.object.isRequired
}
export default Card