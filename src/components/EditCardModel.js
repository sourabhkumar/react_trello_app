import React,{Component} from 'react'
import { cardsref } from '../Firebase'
import PropsTypes from 'prop-types'
import TextareaAutosize from 'react-autosize-textarea'
class EditCardModel extends Component
{
state={
    availablelabels:[
        "#80ccff",
        "#80ffaa",
        "#f94a1e",
        "#ffb3ff",
        "#ffad33"
    ],
    selectedLabels:[]
}
componentDidMount(){
    this.setState({
        selectedLabels:this.props.cardData.labels
    })
}
textInput=React.createRef()

updateCard=async e=>{
try{
e.preventDefault()
const cardId= this.props.cardData.id
const newText=this.textInput.current.value
const labels=this.state.selectedLabels
const card= await cardsref.doc(cardId)
card.update({
    'card.text':newText,
    'card.labels':labels
})
this.props.toggleModel()
}catch(error){
    console.error('Error updating Cards', error)
}
}
setLabel=(label)=>{
    const labels=[...this.state.selectedLabels]
    if(labels.includes(label)){
        const newLabels=labels.filter((element)=>{
            return element!==label
        })
        this.setState({
            selectedLabels:newLabels
        })
    }
    else{
        labels.push(label)
        this.setState({selectedLabels:labels})
    }
}
render(){
return(
    <div className="model-wrapper" style={{display:this.props.modelOpen ? 'block':'none'}}>
        <div className="model-body">
            <form onSubmit={this.updateCard}>  
                <div>
                    <span className="modal-close" onClick={this.props.toggleModel}>&times;</span>
                    <p className="label-title">add / remove labels:</p>
                    {this.state.availablelabels.map(label=>{
                        return <span 
                        onClick={()=>this.setLabel(label)} key={label} className="label" style={{backgroundColor:label}}></span>
                    })}
                    <hr/>
                </div>
                <div className="edit-area">
                    <span className="edit-icon">&#x270E;</span>
                    {/* <input className="textbox-edit"
                    defaultValue={this.props.cardData.text} ref={this.textInput}></input> */}
                    <TextareaAutosize className="textbox-edit"
                    defaultValue={this.props.cardData.text} ref={this.textInput}></TextareaAutosize>
                </div>
                <div>
                    <p className="label-title">labels:</p>
                    {this.state.selectedLabels.map(label=>{
                        return <span className="label" style={{backgroundColor:label}} key={label}></span>
                    })}
                </div>
                <button type="submit">Save Changes</button>
            </form>
        </div>
    </div>
)

}
}
EditCardModel.PropsTypes={
    modelOpen:PropsTypes.bool.isRequired,
    toggleModel:PropsTypes.func.isRequired,
    cardData:PropsTypes.object.isRequired
}

export default EditCardModel