import React,{Component} from 'react'
import Card from './Card'
import propType from'prop-types'
import  {cardsref,listsRef} from '../Firebase'
import  {AuthConsumer} from './AuthContext'
class List extends Component
{
    state={
        currentcards:[]
    }
//     deleteList=async ()=>{
// try{
// const listId=this.props.list.id
// const cards=await cardsref
// .where('card.listId','==',listId)
// .get()
// if(cards.docs.length!==0)
// {
// cards.forEach(card=>{
//     card.ref.delete()
// })
// }
// const list=await listsRef.doc(listId)
// {
//     list.delete()
// }
// }
// catch(error){
//     console.error('Error Deleting List',error)
// }
//     }
    componentDidMount(){
        this.fetchCarrds(this.props.list.id)
    }
    fetchCarrds=async listId=>{
        try{
// const cards=
await cardsref
.where('card.listId','==',listId)
.orderBy('card.createdAt')
.onSnapshot(snapshot=>{
    snapshot.docChanges()
    .forEach(change=>{
        const doc=change.doc
        const card={
            id:doc.id,
            text:doc.data().card.text,
            labels:doc.data().card.labels
        }
        if(change.type==='added'){
            this.setState({currentcards:[...this.state.currentcards,card]})
        }
        if(change.type==='removed'){
            this.setState({
                currentcards:[
                    ...this.state.currentcards.filter(card=>{
                        return card.id!==change.doc.id
                    })
                ]
            })
        }
        if(change.type==='modified'){
            const index=this.state.currentcards.findIndex(item=>{
                return item.id===change.doc.id
            })
            const cards=[...this.state.currentcards]
            cards[index]=card
            this.setState({currentcards:cards})
        }
    })
})
// .get()
// cards.forEach(card=>{
//     const data= card.data().card
//     const cardObj  ={
//         id: card.id,
//         ...data
//     }
//     this.setState({
//         currentcards:[...this.state.currentcards,cardObj]
//     })
// })
        }catch(error){
            console.log('Error Fetching cards',error)
        }
    }
    nameInput=React.createRef()
    crateNewCrad=async (e,userId)=>{
        try{
        e.preventDefault()
        const card={
            text:this.nameInput.current.value,
            listId:this.props.list.id,
            labels:[],
            createdAt:new Date(),
            user:userId
        }
        if(card.text && card.listId){
            await cardsref.add({card})
        }
        this.nameInput.current.value=''
        console.log('neew card added' +card.text)
    } catch(error){console.error('Erroe Created card', error)}
    } 
    deleteList=()=>{
        const listId=this.props.list.id
        this.props.deleteList(listId)
    }
    updateList=async (e)=>{
try{
const listId=this.props.list.id
const newTitle=e.currentTarget.value
const list=await listsRef.doc(listId)
list.update({'list.title':newTitle})
}catch(error){
    console.error('Error Updating List',error)
}
    }
render()
{
return(
    <AuthConsumer>
        {({user})=>(
            <div className="list">
    <div className="list-header">
    {/* <p>{this.props.list.title}</p> */}
    <input
    type="text"
    name="listTitle"
    onChange={this.updateList}
    defaultValue={this.props.list.title}
    ></input>
    <span onClick={this.deleteList}>&times;</span>
    {Object.keys(this.state.currentcards).map((key)=>(
        <Card key={key} data={this.state.currentcards[key]}/>
    ))}
    </div>
    <form onSubmit={(e)=>this.crateNewCrad(e,user.id)} className="new-card-wrapper">
        <input type="text" name="name" placeholder="+Add New Card" ref={this.nameInput}></input>
    </form>
</div>
        )}
    </AuthConsumer>

    
)

}

}
List.propType={
    list:propType.object.isRequired,
    deleteList:propType.func.isRequired
}
export default List