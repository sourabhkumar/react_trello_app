import React,{Component} from 'react'
import  BoardPreview from '../BoardPreviev'
import CreateBoardForm from '../CreateBoardForm'
import propType from'prop-types'
class Home extends Component
{
    componentDidMount()
  {
  this.props.getBoards(this.props.match.params.userID)
  }

render()
{

return(
    
<div>
{/* <span>{this.props.match.params.userID}</span> */}
<CreateBoardForm CreateNewBoard={this.props.createnewBoard}/>
<div className="board-preview-wrapper">
    {Object.keys(this.props.boards).map(key=>(
        <BoardPreview key={key} board={this.props.boards[key]}/>
    ))}
    </div>
</div>
)
}
}
Home.propType={
    getBoards:propType.func.isRequired,
    boards: propType.object.isRequired,
    createnewBoard:propType.func.isRequired
  }
export default Home